package GameState;

import java.awt.Graphics2D;
import java.awt.event.KeyEvent;
import java.util.ArrayList;

import Main.PlayMusic;
import TileMap.FabricaObjetos;
import TileMap.Figura;
import TileMap.Personaje;


public class Level4State extends GameState{
	 	private ArrayList<Figura> lf;
	    private FabricaObjetos fabrica;
	    private Personaje kate;
	    private PlayMusic music;
	    
	    public Level4State(GameStateManager gsm){
		this.gsm=gsm;
		this.lf= new ArrayList<Figura>();
		fabrica = new FabricaObjetos();
		init();
	}
   
	
	@Override
	public void init() {
		lf.clear();
		// TODO Auto-generated methods stub
        lf.add(fabrica.agregaFigura(800, 25, 0, 575, "Piso"));
        lf.add(fabrica.agregaFigura(200, 25, 100, 550, "Azul"));
        lf.add(fabrica.agregaFigura(100, 25, 200, 525, "Azul"));
        lf.add(fabrica.agregaFigura(100, 25, 300, 500, "Verde"));
        lf.add(fabrica.agregaFigura(300, 25, 400, 500, "Azul"));
        lf.add(fabrica.agregaFigura(250, 25, 450, 475, "Verde"));
        lf.add(fabrica.agregaFigura(200, 25, 500, 450, "Azul"));
        lf.add(fabrica.agregaFigura(150, 25, 550, 425, "Azul"));
        lf.add(fabrica.agregaFigura(100, 25, 600, 400, "Azul"));
        lf.add(fabrica.agregaFigura(50, 25, 650, 375, "Azul"));
        lf.add(fabrica.agregaFigura(100, 25, 700, 350, "Verde"));
        lf.add(fabrica.agregaFigura(300, 25, 400, 325, "Azul"));
        lf.add(fabrica.agregaFigura(250, 25, 400, 300, "Verde"));
        lf.add(fabrica.agregaFigura(200, 25, 400, 275, "Azul"));
        lf.add(fabrica.agregaFigura(150, 25, 400, 250, "Verde"));
        lf.add(fabrica.agregaFigura(100, 25, 400, 225, "Azul"));
        lf.add(fabrica.agregaFigura(50, 25, 400, 200, "Verde"));
        lf.add(fabrica.agregaFigura(100, 25, 300, 200, "Azul"));
        lf.add(fabrica.agregaFigura(400, 25, 0, 325, "Azul"));
        lf.add(fabrica.agregaFigura(20, 20, 0, 305, "Win"));
        System.out.println(lf.toString());
        kate=new Personaje();
        music=new PlayMusic();
        music.music();
	}

	@Override
	public void update() {
		// TODO Auto-generated method stub
		choca();
		kate.move();
		kate.outOfBounds();
//		System.out.println(kate.getPosX());
	}

	@Override
	public void draw(Graphics2D g) {
		// TODO Auto-generated method stub
		for(Figura f:lf){
			if(f.isColision()){
				f.pintate(g);
			}
		}
		kate.pintate(g);
	}

	@Override
	public void keyPressed(int e) {
       if(e ==KeyEvent.VK_1){
       	music.sound("1");
       	cambiarCol();
       }
       if (e == KeyEvent.VK_2){
       	music.sound("2");
       	cambiarCol();
       }
       //Moverse derecha
       if(e == KeyEvent.VK_RIGHT){
    	   kate.setVelX(2);
//    	   for(Figura f: lf){
//    		   if(f.intersects(kate)&&f.isColision()){
//    			   if(kate.getPosX()+kate.getWidth()>f.getPosX()){
//    				   if(kate.getPosY()+kate.getHeight()-2>f.getPosY()){
//       						System.out.println(f.toString());
//       						kate.setPosX(f.getPosX()-kate.getWidth());
//       						kate.setVelX(0);
//       					}else{
//       						
//       					}
//       				}  kate.setVelX(2);     			
//       			}
//       		}
       }
       //Moverse izquierda
       if(e == KeyEvent.VK_LEFT){
//       	lf.colisiones(kate);
//       	System.out.println("izquierda");
       	//if (chocaLados(keyCode))
       	kate.setVelX(-2);
//       	for(Figura f: lf){
//       		if(f.intersects(kate)&&f.isColision()){
//       			if(keyCode==KeyEvent.VK_LEFT){
//       				if(kate.getPosX()<f.getPosX()+f.getWidth()){
//       					if(kate.getPosY()+kate.getHeight()-2>f.getPosY()){
//       						kate.setVelX(0);
//       						System.out.println(f.toString());
//       					}else{
//       						kate.setVelX(-2);
//       					}
//       				}
//       					
//       			}
//       		}
//       	}
       }
       //Saltar
       if(e == KeyEvent.VK_SPACE){
//       	lf.colisiones(kate);
//       	System.out.println("salta");
       	//     	if (chocaLados(keyCode))
//       	boolean i=false;
//       	for(Figura f: lf){
//       		if(f.intersects(kate)&&kate.getPosY()<=f.getPosY()+f.getHeight()){
//       			i=true;
//       		}
//       	}
//       	if(!i){
//       		kate.jump();
//       	}
       	kate.jump();
//       	chocaLados(keyCode);
       }
	}
    public void cambiarCol(){
    	for(Figura f: lf){
    		if(f.isColision()&&!(f.getTipo()=="Piso")&&!(f.getTipo()=="Win")){
    			f.setColision(false);
    		}else{
    			f.setColision(true);
    		}
    	}
    }
	@Override
	public void keyReleased(int e) {
		// TODO Auto-generated method stub
    	//Moverse derecha
        if(e == KeyEvent.VK_RIGHT){
//        	lf.colisiones(kate);
//        	System.out.println("derecha");
        	kate.setVelX(0);
        	
        }
        //Moverse izquierda
        if(e == KeyEvent.VK_LEFT){
//        	lf.colisiones(kate);
//        	System.out.println("izquierda");
        	kate.setVelX(0);
        }
        //Saltar
        if(e == KeyEvent.VK_SPACE){
//        	lf.colisiones(kate);
//        	System.out.println("salta");
        	kate.jump();
        }
	}
    public void choca(){
    	boolean i=true;
    	for(Figura f: lf){
			if(f.isColision()&&f.intersects(kate)){
//				kate.setFalling(false);
				if(f.getTipo()=="Win"){
					gsm.setState(0);
				}
				i=false;
//				System.out.println(f.toString());
			}
			else if(!f.isColision()&&f.intersects(kate)){
				kate.setFalling(true);
			}
			else if(!f.intersects(kate)){
				kate.setFalling(true);
			}
			if(i){
				kate.setFalling(true);
			}else {
				kate.setFalling(false);
			}
		}
    	
//    	System.out.println(kate.getFalling());
    }

    
}