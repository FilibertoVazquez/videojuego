package GameState;

import TileMap.*;

import java.awt.event.KeyEvent;
import java.awt.image.*;
import java.util.ArrayList;
import java.awt.*;

import Main.GamePanel;

public class Level1State extends GameState implements ImagesPlayerWatcher, ImageObserver{
	
	//private TileMap tileMap;
	private ArrayList<Figura> lf;
	private FabricaObjetos fabrica;
	private Background bg, bg2;
	private int flag = 0;
	private ImagesLoader imsLoader;
	private ImagesPlayer background, backgroundToNight;
	 
	
	public Level1State(GameStateManager gsm){
		
		bg = new Background("/Backgrounds/background0.gif",1);
		bg2 = new Background("/Backgrounds/background-change0.gif",1);
		this.gsm = gsm;
		fabrica = new FabricaObjetos();
		this.lf= new ArrayList<Figura>();
		imsLoader = new ImagesLoader(GamePanel.IMS_FILE);
		initImages();
	}

	@Override
	public void init() {
		lf.clear();
		// TODO Auto-generated methods stub
        lf.add(fabrica.agregaFigura(800, 25, 0, 575, "Piso"));
        lf.add(fabrica.agregaFigura(200, 25, 100, 550, "Azul"));
        lf.add(fabrica.agregaFigura(100, 25, 200, 525, "Azul"));
        lf.add(fabrica.agregaFigura(100, 25, 300, 500, "Verde"));
        lf.add(fabrica.agregaFigura(300, 25, 400, 500, "Azul"));
        lf.add(fabrica.agregaFigura(250, 25, 450, 475, "Verde"));
        lf.add(fabrica.agregaFigura(200, 25, 500, 450, "Azul"));
        lf.add(fabrica.agregaFigura(150, 25, 550, 425, "Azul"));
        lf.add(fabrica.agregaFigura(100, 25, 600, 400, "Azul"));
        lf.add(fabrica.agregaFigura(50, 25, 650, 375, "Azul"));
        lf.add(fabrica.agregaFigura(100, 25, 700, 350, "Verde"));
        lf.add(fabrica.agregaFigura(300, 25, 400, 325, "Azul"));
        lf.add(fabrica.agregaFigura(250, 25, 400, 300, "Verde"));
        lf.add(fabrica.agregaFigura(200, 25, 400, 275, "Azul"));
        lf.add(fabrica.agregaFigura(150, 25, 400, 250, "Verde"));
        lf.add(fabrica.agregaFigura(100, 25, 400, 225, "Azul"));
        lf.add(fabrica.agregaFigura(50, 25, 400, 200, "Verde"));
        lf.add(fabrica.agregaFigura(100, 25, 300, 200, "Azul"));
        lf.add(fabrica.agregaFigura(400, 25, 0, 325, "Azul"));
        lf.add(fabrica.agregaFigura(20, 20, 0, 305, "Win"));
	}

	@Override
	public void update() {
		// TODO Auto-generated method stub
		imagesUpdate();
		//System.out.println(flag);
		
	}

	@Override
	public void draw(Graphics2D g) {
		// clear screen
		//g.setColor(Color.white);
		g.fillRect(0, 0, GamePanel.WIDTH, GamePanel.HEIGHT);
		if(flag == 0){
        	bg.draw(g);
        }
        if(flag == 1){
        	drawImage(g, background.getCurrentImage(), 0, 0);
        }
        if (flag == 2){
        	drawImage(g,backgroundToNight.getCurrentImage(),0,0);
        }
		
		//draw Figures
		for(Figura f:lf){
			if(f.isColision()){
				f.pintate(g);
			}
		}
	}
	
	private void drawImage(Graphics2D g, BufferedImage im, int x, int y) {
		{
	        if (im == null) {
	            // System.out.println("Null image supplied");
	            g.setColor(Color.yellow);
	            g.fillRect(x, y, 20, 20);
	            g.setColor(Color.black);
	            g.drawString("??", x+10, y+10);
	        }
	        else
	        	g.drawImage(im, x, y, this);
	    }
		
	}

	public void cambiarCol(){
    	for(Figura f: lf){
    		if(f.isColision()&&!(f.getTipo()=="Piso")&&!(f.getTipo()=="Win")){
    			f.setColision(false);
    		}else{
    			f.setColision(true);
    		}
    	}
    }

	@Override
	public void keyPressed(int k) {
		if(k == KeyEvent.VK_1){
			cambiarCol();
			//System.out.println(flag);
			if(flag == 0){
				flag = 1;
				background.restartAt(0);
			}else if(flag == 1){
				//System.out.println("entro");
				flag = 2;
				backgroundToNight.restartAt(0);
			}else if(flag == 2){
				flag = 1;
				background.restartAt(0);
			}
		}
		
	}

	@Override
	public void keyReleased(int k) {
		// TODO Auto-generated method stub
		
	}
	
	 private void initImages(){
		 background = new ImagesPlayer("background", GamePanel.PERIOD, 2, false, imsLoader);
	     backgroundToNight = new ImagesPlayer("background-change", GamePanel.PERIOD, 2.1, false, imsLoader);

	    }
	 
	 private void imagesUpdate()
	    {
	    	if(flag == 1){
	    		background.updateTick();
	    	}
	    	if (flag == 2){
	    		backgroundToNight.updateTick();
	    	}
	    	


	    }

	@Override
	public void sequenceEnded(String imageName) {
		// TODO Auto-generated method stub
		
	}

	public Background getBg2() {
		return bg2;
	}

	public void setBg2(Background bg2) {
		this.bg2 = bg2;
	}

	@Override
	public boolean imageUpdate(Image img, int infoflags, int x, int y, int width, int height) {
		// TODO Auto-generated method stub
		return false;
	}

}
