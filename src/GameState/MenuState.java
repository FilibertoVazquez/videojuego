package GameState;

import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics2D;
import java.awt.event.KeyEvent;

import TileMap.Background;

public class MenuState extends GameState{

	private Background bg;
	
	private int currentChoice = 0;
	private String[] options = {
			"Start",
			"Help",
			"Quit"
	};
	
	private Color titleColor;
	private Font font;
	
	public MenuState(GameStateManager gsm){
		this.gsm = gsm;
		
		try{
			bg = new Background("/Backgrounds/Fondo copy.png",1);

			titleColor = new Color(247,206,82);
			font = new Font ("Futura", Font.BOLD,48);
		}catch(Exception e){
			e.printStackTrace();
		}
	}

	@Override
	public void init() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void update() {
		
	}

	@Override
	public void draw(Graphics2D g) {
		//draw bg
		bg.draw(g);
		
		//draw menu options
		g.setFont(font);
		for(int i = 0; i<options.length; i++){
			if(i == currentChoice){
				g.setColor(titleColor);
			}else{
				g.setColor(Color.white);
			}
			g.drawString(options[i], 350, 300+i*84);
		}
		
	}
	
	private void select(){
		if(currentChoice == 0){
			//start
			gsm.setState(GameStateManager.LEVEL1STATE);
		}
		if(currentChoice == 1){
			//help
		}
		if(currentChoice == 2){
			System.exit(0);
		}
	}

	@Override
	public void keyPressed(int k) {
		if(k == KeyEvent.VK_ENTER){
			select();
		}
		if(k == KeyEvent.VK_UP){
			currentChoice --;
			if(currentChoice == -1){
				currentChoice = options.length -1;
			}
			
		}
		if(k == KeyEvent.VK_DOWN){
			currentChoice ++;
			if(currentChoice == options.length){
				currentChoice = 0;
			}
			
		}
		
	}

	@Override
	public void keyReleased(int k) {
		// TODO Auto-generated method stub
		
	}
}
