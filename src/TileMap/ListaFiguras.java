package TileMap;

import java.awt.Graphics;

import java.util.ArrayList;

public class ListaFiguras {

	private ArrayList<Figura>figuras;
	
	public ListaFiguras(){
		figuras = new ArrayList<Figura>();
	}

	public ArrayList<Figura> getFiguras() {
		return figuras;
	}

	public void setFiguras(ArrayList<Figura> figuras) {
		this.figuras = figuras;
	}
	
	public void addFig(Figura f){
		figuras.add(f);
	}
	
	public void paint(Graphics g){
		for(Figura f: figuras){
			if(f.isColision()==true){
				f.pintate(g);
			}
		}
	}
	
	@Override
	public String toString() {
		return "ListaFiguras [figuras=" + figuras + "]";
	}

	public void colisiones(Personaje kate)
	{		
		for(Figura f: figuras)
		{
			if(f.isColision()&&f.intersects(kate))
			{
				kate.setFalling(false);
			}
			else if(!f.isColision()&&f.intersects(kate))
			{
				kate.setFalling(true);
			}
		}
		if(kate.getFalling()){
			kate.move();
		}
	}
	
	public void cambiarCol(){
    	for(Figura f: figuras){
    		if(f.isColision()&&!(f.getTipo()=="Piso")){
    			f.setColision(false);
    		}else{
    			f.setColision(true);
    		}
    	}
    }
	
}
