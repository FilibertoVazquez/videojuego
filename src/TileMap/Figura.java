package TileMap;

import java.awt.*;
import java.util.Random;

public class Figura
{
    protected int width;
    protected int height;
    protected int posX;
    protected int posY;
    protected Color color;
    protected String tipo;
    protected boolean colision;

    public Figura(int width, int height, int posX, int posY, Color color, String tipo, boolean colision) {
		super();
		this.width = width;
		this.height = height;
		this.posX = posX;
		this.posY = posY;
		this.color = color;
		this.tipo = tipo;
		this.colision = colision;
	}
    public Figura(){
    	this.width = 0;
		this.height = 0;
		this.posX = 0;
		this.posY = 0;
		this.color = Color.BLACK;
		this.tipo = "Piso";
		this.colision = true;
    }

	public String getTipo() {
		return tipo;
	}

	public void setTipo(String tipo) {
		this.tipo = tipo;
	}

	public Figura(int w, int h, int x, int y, String tipo)
    {
    	if(tipo=="Piso"){
	        width = w;
	        height = h;
	        posX = x;
	        posY = y;
	        color = new Color(200,200,200);
	        colision = true;
	        this.tipo=tipo;
    	}else if(tipo=="Azul"){
    		width = w;
        	height = h;
        	posX = x;
        	posY = y;
        	color = new Color(31,244,247);
        	colision = false;
        	this.tipo=tipo;
    	}else if(tipo=="Win"){
        	width = w;
        	height = h;
        	posX = x;
        	posY = y;
        	color = new Color(255,215,0);
        	colision = true;
        	this.tipo=tipo;
    	}
    	else{
        	width = w;
        	height = h;
        	posX = x;
        	posY = y;
        	color = new Color(0,244,0);
        	colision = true;
        	this.tipo=tipo;
    	}
    }
    
    public Figura(int w, int h, int x, int y){
    	width = w;
    	height = h;
    	posX = x;
    	posY = y;
    	color = new Color(200,200,200);
    	colision = true;
    }

    public void pintate(Graphics g) {
    	g.setColor(color);
	    g.fillRect(this.getPosX(), getPosY(), this.getWidth(), this.getHeight());
	}


    public int getPosX() {
        return posX;
    }

    public int getPosY() {
        return posY;
    }

    public void setPosX(int posX) {
        this.posX = posX;
    }

    public void setPosY(int posY) {
        this.posY = posY;
    }
    
    public Color getColor(){
    	return color;
    }
    
    public void setColor(Color color){
    	this.color = color;
    }

	public int getWidth() {
		return width;
	}

	public void setWidth(int width) {
		this.width = width;
	}

	public int getHeight() {
		return height;
	}

	public void setHeight(int height) {
		this.height = height;
	}

	public boolean isColision() {
		return colision;
	}

	public void setColision(boolean colision) {
		this.colision = colision;
	}
	public Rectangle getRectangle() {
		  return new Rectangle(posX,posY,width,height);
		 }
	public boolean intersects(Personaje kate) {
		  Rectangle r1 = getRectangle();
		  Rectangle r2 = kate.getRectangle();
		  return r1.intersects(r2);
		 }

	@Override
	public String toString() {
		return "Figura [width=" + width + ", height=" + height + ", posX=" + posX + ", posY=" + posY + ", color="
				+ color + ", colision=" + colision + "]";
	}
    
    
}

