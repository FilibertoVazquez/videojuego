import java.awt.Graphics; 
import java.util.LinkedList; 
 
public class ListaPersonajes { 
 
  private LinkedList<Personaje>personajes; 
   
  public ListaPersonajes(){ 
    personajes = new LinkedList<Personaje>(); 
  } 
 
  public LinkedList<Personaje> getPersonaje() { 
    return personajes; 
  } 
 
  public void setFiguras(LinkedList<Personaje> figuras) { 
    this.personajes = figuras; 
  } 
   
  public void agregaPersonaje(Personaje f){ 
    personajes.add(f); 
  } 
   
  public void paint(Graphics g){ 
    for(Personaje p: personajes){ 
      p.pintate(g); 
    } 
  } 
}