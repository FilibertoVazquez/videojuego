import java.awt.*;
import java.util.ArrayList; 
import java.util.Random; 
 
public class Personaje implements PersonajeState
{ 
    protected int width; 
    protected int height; 
    protected int posX; 
    protected int posY; 
    private int velX;
    protected int orientacion; 
    protected Color color; 
    protected boolean colision; 
    public boolean falling;
    protected int accY, gravity;
     
    public Personaje(){ 
        width = 10; 
        height = 10; 
        posX = 50; 
        posY = 500; 
        /*color = new Color(200,200,200);*/ 
        color = Color.BLUE; 
        colision = true; 
        accY=0;
        gravity=1;
        falling=false;
        
    } 
     
    public Personaje(int w, int h, int x, int y, boolean col) 
    { 
        width = w; 
        height = h; 
        posX = x; 
        posY = y; 
        /*color = new Color(200,200,200);*/ 
        color = new Color(31,244,247); 
        colision = col; 
        accY=0;
        gravity=1;
        falling=false;
    } 
    public void move() {
    	if(this.falling){
	    	if(this.accY<1){
	    		    this.accY+=1;
	    	}
	    	
	    		this.setPosY(this.getPosY()+accY+gravity);
	    	
    	}else{
    		this.setPosY(this.getPosY());
//    		System.out.println("HELP");
    	}
    	this.posX+=velX;
//    	System.out.println(accY);
    }
 
    public int getVelX() {
		return velX;
	}

	public void setVelX(int velX) {
		this.velX = velX;
	}

	public int getPosX() { 
        return posX; 
    } 
 
    public int getPosY() { 
        return posY; 
    } 
 
    public void setPosX(int posX) { 
        this.posX = posX; 
    } 
 
    public void setPosY(int posY) { 
        this.posY = posY; 
    } 
     
    public Color getColor(){ 
      return color; 
    } 
     
    public void setColor(Color color){ 
      this.color = color; 
    }

	public int getWidth() {
		return width;
	}
	public void jump() {
		// TODO Auto-generated method stub
		if(this.falling){
			
		}else{
			this.setAccY(-10);
			if(this.accY<1){
				this.accY+=.1;
			}
			this.setPosY(this.getPosY()+accY+gravity);
		}
	}
	public void setFalling(boolean b){
		falling=b;
	}
	public boolean getFalling(){
		return falling;
	}

	public boolean isColision() {
		return colision;
	}

	public void setColision(boolean colision) {
		this.colision = colision;
	}

	public int getAccY() {
		return accY;
	}

	public void setAccY(int accY) {
		this.accY = accY;
	}

	public int getGravity() {
		return gravity;
	}

	public void setGravity(int gravity) {
		this.gravity = gravity;
	}

	public void setOrientacion(int orientacion) {
		this.orientacion = orientacion;
	}

	public void setWidth(int width) {
		this.width = width;
	}

	public int getHeight() {
		return height;
	}

	public void setHeight(int height) {
		this.height = height;
	}

	public int getOrientacion() {
		return 0;
	}


	public void movex(int x) {
		this.setPosX(getPosX()+x);
	} 
	
	public Rectangle getRectangle() {
		  return new Rectangle(posX,posY,width,height);
		 }
	 public void pintate(Graphics g)  
	  { 
	    g.setColor(color.RED); 
	      g.fillRect(this.getPosX(), getPosY(), this.getWidth(), this.getHeight()); 
	  }

	@Override
	public void still() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void movey() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public boolean fall() {
		return colision;
		
	}

	@Override
	public void dir() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void draw(Graphics g) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void checkState() {
		// TODO Auto-generated method stub
		
	}
	public void outOfBounds(){
		if(this.getPosX()<0){
			this.setPosX(0);
		}
		if(this.getPosX()>800)
		if(this.getPosY()>600){
			this.setPosX(50);
			this.setPosY(500);
		}
	}
}
