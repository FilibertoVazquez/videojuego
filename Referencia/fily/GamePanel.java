
// ImagesTests.java
// Andrew Davison, April 2005, ad@fivedots.coe.psu.ac.th

/* Display a screen-full of images, exhibiting various animated
   effects.

   The images are loaded with the ImagesLoader object, so are
   defined as 'o', 'n', 's' and 'g' images.

   The single images (the 'o' images)
   can have various special effects applied to them. This is done
   by the programmer _manually_ editing paintComponent(). Sorry,
   no fancy GUI in this program :)

   The special effects are applied over a period of several 'ticks'
   of the panel's timer, and then repeat. The SFX methods:

      * resizingImage():   the image grows;
      * flippingImage():   keep flipping the image horizontally and vertically;
      * fadingImage():     the image smoothly fades away to nothing;
      * rotatingImage():   spin the image in a clockwise direction;
      * blurringImage():   make the image increasingly more blurred;
      * reddenImage():     turn the image ever more red;
      * brighteningImage():  keep turning up the image's brightness;
      * negatingImage():   keep switching betwen the image and its negative;
      * mixedImage();      keep mixing up the colours of the image;
      * teleportImage():   make the image fade, pixels at a time;
      * zapImage():        change the image to a mass of yellow and red pixels;

   ------
   The 'n', 's', and 'g' images are animated by
   showing their component images one after another, in a cycle.
   The 'n' and 's' images use ImagesPlayer objects to do this.

   The 'g' example (the 'fighter) is animated via a counter and
   the updateFighter() method in this class.

   A Swing Timer is used to trigger the
   updates and redraws of the images every PERIOD ms.

   The 'cats', 'kaboom', 'cars', and 'fighter' images come from
   the SpriteLib sprite library of images by Ari Feldman at
   http://wwpiso.arifeldman.com/games/spritelib.html

   The basn6a08.png and basn6a16.png images come from the PNG Suite
   of Willem van Schaik at http://wwpiso.schaik.com/pngsuite/pngsuite.html
*/

import java.awt.*;
import java.awt.event.*;
import java.awt.image.*;
import javax.swing.*;

import sun.audio.AudioPlayer;
import sun.audio.AudioStream;
import sun.audio.ContinuousAudioDataStream;

import javax.imageio.*;
import java.io.*;
import java.text.DecimalFormat;


public class GamePanel extends JPanel
        implements
        ImagesPlayerWatcher, KeyListener, Runnable
{
    /**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private final static String IMS_FILE = "imsInfo.txt";
    /* The file holding the 'o', 'n', 's', and 'g' image information,
       extracted with an ImagesLoader object. */

    private static final int PERIOD = 100;    // 0.1 secs
      /* A Swing timer is triggered every PERIOD ms to update
         and redraw the images. */

    private Thread animator; //controla la animación
    private volatile boolean running = false;
    private volatile boolean gameOver = false;
    private volatile boolean isPaused = false;
    private ListaFiguras lf;
//    private ListaPersonajes lp; 
    private FabricaObjetos fabrica;
    private PersonajeState ps;
    private int change;

    private static final int PWIDTH = 800;     // size of this panel
    private static final int PHEIGHT = 600;

    private ImagesLoader imsLoader;   // the image loader
    private int counter;
    private boolean justStarted;

    private GraphicsDevice gd;   // for reporting accl. memory usage
    private int accelMemory;
    private DecimalFormat df;
    private Personaje kate;
    
    private Figura piso;

    // for manipulating the 'n' and 's' images
    private ImagesPlayer background, backgroundToNight;
    private BufferedImage night;


    public GamePanel()
    {
    	change = 0;
    	lf = new ListaFiguras();
    	//lp = new ListaPersonajes(); 
        df = new DecimalFormat("0.5");  // 1 dp

        GraphicsEnvironment ge = GraphicsEnvironment.getLocalGraphicsEnvironment();
        gd = ge.getDefaultScreenDevice();

        accelMemory = gd.getAvailableAcceleratedMemory();  // in bytes
        System.out.println("Initial Acc. Mem.: " +
                df.format( ((double)accelMemory)/(1024*1024) ) + " MB" );

        setBackground(Color.white);
        setPreferredSize( new Dimension(PWIDTH, PHEIGHT) );

        // load and initialise the images
        imsLoader = new ImagesLoader(IMS_FILE);
        initImages();

        counter = 0;
        justStarted = true;
        setFocusable(true);
        requestFocus();
        readyForTermination();
        addKeyListener(this);
        
        fabrica = new FabricaObjetos();
        //lf.agregaFigura(fabrica.agregaFigura(100,50, 150,150,true));
        //Piso:
        //Figuras f = new Figuras(w,h, x, y, col);
        
        /*NIVEL 1*/
        piso=fabrica.agregaFigura(800, 25, 0, 575);
        lf.agregaFigura(piso);
        lf.agregaFigura(fabrica.agregaFigura(200, 25, 100, 550, true));
        lf.agregaFigura(fabrica.agregaFigura(100, 25, 200, 525, true));
        lf.agregaFigura(fabrica.agregaFigura(100, 25, 300, 500));
        lf.agregaFigura(fabrica.agregaFigura2(300, 25, 400, 500, true));
        lf.agregaFigura(fabrica.agregaFigura2(250, 25, 450, 475, true));
        lf.agregaFigura(fabrica.agregaFigura2(200, 25, 500, 450, true));
        lf.agregaFigura(fabrica.agregaFigura2(150, 25, 550, 425, true));
        lf.agregaFigura(fabrica.agregaFigura2(100, 25, 600, 400, true));
        lf.agregaFigura(fabrica.agregaFigura2(50, 25, 650, 375, true));
        lf.agregaFigura(fabrica.agregaFigura(100, 25, 700, 350));
        lf.agregaFigura(fabrica.agregaFigura(300, 25, 400, 325, true));
        lf.agregaFigura(fabrica.agregaFigura(250, 25, 400, 300, true));
        lf.agregaFigura(fabrica.agregaFigura(200, 25, 400, 275, true));
        lf.agregaFigura(fabrica.agregaFigura(150, 25, 400, 250, true));
        lf.agregaFigura(fabrica.agregaFigura(100, 25, 400, 225, true));
        lf.agregaFigura(fabrica.agregaFigura(50, 25, 400, 200, true));
        lf.agregaFigura(fabrica.agregaFigura(100, 25, 300, 200));
        lf.agregaFigura(fabrica.agregaFigura2(400, 25, 0, 325, true));
        
        
      //PERSONAJE 
        kate=fabrica.agregaPersonaje(20, 20, 10, 549, true, null, null, null, null); 



        //lf.agregaFigura(fabrica.agregaFigura(300, 25, 500, 225, true));

    } // end of ImagesTests()

    public void addNotify()
    {
        super.addNotify();
        startGame();
    }//addNotify

    private void startGame()
    {
        if(animator == null || !running)
        {
            animator = new Thread(this);
            animator.start();
            music();
        }
    }//startGame()

    public void stopGame(){
        running = false;
    }//stopGame()

    public void run(){
        running = true;
        while(running)
        {
            gameUpdate();
            gameRender();
            paintScreen();
            
            //kate.move();
            if(lf.colisiones(kate, 4)&&kate.getPosY()+kate.getHeight()>this.HEIGHT&&kate.getPosX()+kate.getWidth()>this.WIDTH)
            {
            	kate.move();
            }
       

            try{
                Thread.sleep(10);
            }catch(InterruptedException ex){}
        }
        System.exit(0);
    }//run()

    private void gameUpdate(){

        if(!isPaused && !gameOver)
        {
            if (justStarted)   // don't do updates the first time through
                justStarted = false;
            
            imagesUpdate();
            repaint();
        }

    }//gameUpdate()

    private Graphics dbg;
    private Image dbImage = null;

    private void gameRender(){
        if(dbImage == null){
            dbImage = createImage(PWIDTH,PHEIGHT);
            if(dbImage == null)
            {
                System.out.println("dbImage is null");
                return;
            }
            else
            {
                dbg = dbImage.getGraphics();
            }
            dbg.setColor(Color.white);
            dbg.fillRect(0,0,PWIDTH,PHEIGHT);
        }
        else
        {
            dbg.setColor(Color.white);
            dbg.fillRect(0,0,PWIDTH,PHEIGHT);
            //lf.paint(dbg);
        }
    }//gameRender()

    private void gameOverMessage()
    {
        Graphics g;
        g=this.getGraphics();
        g.drawString("GameOver",10,10);
    }
    
    /*public void colisiones()
	{	
    	
	}*/
    
    private void readyForTermination() {
        addKeyListener( new KeyAdapter() { // listen for esc, q, end, ctrl-c
            public void keyPressed(KeyEvent e) {
                int keyCode = e.getKeyCode();
                
                if ((keyCode == KeyEvent.VK_ESCAPE) ||
                        (keyCode == KeyEvent.VK_Q) ||
                        (keyCode == KeyEvent.VK_END) ||
                        ((keyCode == KeyEvent.VK_C) && e.isControlDown()) ) {
                    running = false;
                }
                //Cambio a dia
                if(keyCode ==KeyEvent.VK_1){
                	change = 1;
                	sound("1");
                	background.restartAt(0);
                }
                //Cambio a noche
                if (keyCode == KeyEvent.VK_2){
                	change = 2;
                	sound("2");
                	backgroundToNight.restartAt(0);
                }
                //Moverse derecha
                if(keyCode == KeyEvent.VK_RIGHT){
                	lf.colisiones(kate,1);
                	System.out.println("derecha");
                	kate.movex(5);
                	
                }
                //Moverse izquierda
                if(keyCode == KeyEvent.VK_LEFT){
                	lf.colisiones(kate,2);
                	System.out.println("izquierda");
                	kate.movex(-5);
                }
                //Saltar
                if(keyCode == KeyEvent.VK_SPACE){
                	lf.colisiones(kate,3);
                	System.out.println("salta");
                	kate.jump();
                }
            }
        });
    } // end of readyForTermination()

    @Override
    public void keyTyped(KeyEvent e)
    {

    }

    @Override
    public void keyPressed(KeyEvent e)
    {

    }

    @Override
    public void keyReleased(KeyEvent e)
    {

    }

    private void paintScreen(){
        Graphics g;
        try{
            g = this.getGraphics();
            if((g != null) && (dbImage != null))
                g.drawImage(dbImage,0,0,null);
            Toolkit.getDefaultToolkit().sync();
            g.dispose();
        }
        catch(Exception e){
            System.out.println("Graphics context error: "+e);
        }
    }

    public void pauseGame(){
        isPaused = true;
    }

    public void resumeGame(){
        isPaused = false;
    }

    private void initImages()
    {

    	night = imsLoader.getImage("night");
        background =
                new ImagesPlayer("background", PERIOD, 2, false, imsLoader);
        background.setWatcher(this);
        backgroundToNight = new ImagesPlayer("background-change", PERIOD, 2.1, false, imsLoader);
        backgroundToNight.setWatcher(this);

    }  // end of initImages()


    public void sequenceEnded(String imageName)
    // called by ImagesPlayer when its images sequence has finished
    {  System.out.println( imageName + " sequence has ended");  }



    private void imagesUpdate()
    {
    	if(change == 1){
    		background.updateTick();
    	}
    	if (change == 2){
    		backgroundToNight.updateTick();
    	}
    	


    } // end of imagesUpdate()



    public void paintComponent(Graphics g)
    {
        super.paintComponent(g);
        Graphics2D g2d = (Graphics2D)g;

        if(dbImage != null)
        {
            g.drawImage(dbImage, 0, 0, null);
        }

        // use antialiasing
        g2d.setRenderingHint(RenderingHints.KEY_ANTIALIASING,
                RenderingHints.VALUE_ANTIALIAS_ON);

        // smoother (and slower) image transformations  (e.g. for resizing)
        g2d.setRenderingHint(RenderingHints.KEY_INTERPOLATION,
                RenderingHints.VALUE_INTERPOLATION_BILINEAR);




        if(change == 0){
        	drawImage(g2d,night,0,0);
        }
        if(change == 1){
        drawImage(g2d, background.getCurrentImage(), 0, 0);
        }
        if (change == 2){
        	drawImage(g2d,backgroundToNight.getCurrentImage(),0,0);
        }
        lf.paint(g2d);
        kate.pintate(g2d); 
        
        reportAccelMemory();

        // increment the counter, modulo 100
        counter = (counter + 1)% 100;    // 0-99 is a large enough range
        
    } // end of paintComponent()



    private void drawImage(Graphics2D g2d, BufferedImage im, int x, int y)
   /* Draw the image, or a yellow box with ?? in it if there is no image. */
    {
        if (im == null) {
            // System.out.println("Null image supplied");
            g2d.setColor(Color.yellow);
            g2d.fillRect(x, y, 20, 20);
            g2d.setColor(Color.black);
            g2d.drawString("??", x+10, y+10);
        }
        else
            g2d.drawImage(im, x, y, this);
    } // end of drawImage()



    private void reportAccelMemory()
    // report any change in the amount of accelerated memory
    {
        int mem = gd.getAvailableAcceleratedMemory();   // in bytes
        int memChange = mem - accelMemory;

        if (memChange != 0)
            System.out.println(counter + ". Acc. Mem: " +
                    df.format( ((double)accelMemory)/(1024*1024) ) + " MB; Change: " +
                    df.format( ((double)memChange)/1024 ) + " K");
        accelMemory = mem;
    }  // end of reportAcceleMemory()


    public static void main(String args[])
    {
        // switch on translucency acceleration in Windows
        System.setProperty("sun.java2d.translaccel", "true");
        // System.setProperty("sun.java2d.ddforcevram", "true");

        // switch on hardware acceleration if using OpenGL with pbuffers
        // System.setProperty("sun.java2d.opengl", "true");

        GamePanel ttPanel = new GamePanel();

        // create a JFrame to hold the test JPanel
        JFrame app = new JFrame("Image Tests");
        app.getContentPane().add(ttPanel, BorderLayout.CENTER);
        app.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

        app.pack();
        app.setResizable(false);
        app.setVisible(true);
    } // end of main()

	public ListaFiguras getLf() {
		return lf;
	}

	public void setLf(ListaFiguras lf) {
		this.lf = lf;
	}

	public FabricaObjetos getFabrica() {
		return fabrica;
	}

	public void setFabrica(FabricaObjetos fabrica) {
		this.fabrica = fabrica;
	}

	public int getChange() {
		return change;
	}

	public void setChange(int change) {
		this.change = change;
	}
	public void music()
	{
		 AudioPlayer MGP = AudioPlayer.player;
	     AudioStream BGM;
	     //AudioData MD;

	     ContinuousAudioDataStream loop = null;

	     try
	     {
	    	 InputStream test = new FileInputStream("sounds/music.wav");
		     BGM = new AudioStream(test);
		     AudioPlayer.player.start(BGM);
//		     MD = BGM.getData();
//		     loop = new ContinuousAudioDataStream(MD);

		  }
		  catch(FileNotFoundException e)
		  {
		     System.out.print(e.toString());
		  }
		  catch(IOException error)
		  {
		     System.out.print(error.toString());
		  }
		  MGP.start(loop);
	}
	
	public void sound(String s)
	{
		 AudioPlayer MGP = AudioPlayer.player;
	     AudioStream BGM;
	     //AudioData MD;

	     ContinuousAudioDataStream loop = null;

	     try
	     {
	    	 InputStream test = new FileInputStream("sounds/"+s+".wav");
		     BGM = new AudioStream(test);
		     AudioPlayer.player.start(BGM);
//		     MD = BGM.getData();
//		     loop = new ContinuousAudioDataStream(MD);

		  }
		  catch(FileNotFoundException e)
		  {
		     System.out.print(e.toString());
		  }
		  catch(IOException error)
		  {
		     System.out.print(error.toString());
		  }
		  MGP.start(loop);
	}

} // end of ImagesTests class
