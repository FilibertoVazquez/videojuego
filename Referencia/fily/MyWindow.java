/*
Filiberto Francisco Vázquez Rodríguez
A01206199
04/04/16	
*/

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Graphics;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.Font;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JTextField;

public class MyWindow extends JFrame implements ActionListener
{
//	Define the instance variables:

	private MyCanvas myCanvas;
	private JTextField text;
	private JButton b1;
	private JButton b2;
	private JPanel panel;
	
	
	public MyWindow()
	{
		super();
//		Create layout
		setLayout(new BorderLayout());
		
		b1 = new JButton("START");
		b1.setBackground(Color.YELLOW);
        b1.setForeground(new Color(59, 89, 182));
        b1.setFocusPainted(false);
        b1.setFont(new Font("Tahoma", Font.BOLD, 12));
		b2 = new JButton("EXIT");
		text = new JTextField("");

//		Add action listeners:
		b1.addActionListener(this);
		b2.addActionListener(this);
		text.addActionListener(this);
		
		panel= new JPanel();
		panel.add(b1);
		panel.add(b2);
		panel.add(text);
		
		myCanvas = new MyCanvas();
		
		add(text,BorderLayout.NORTH);
		add(myCanvas,BorderLayout.CENTER);
		add(panel,BorderLayout.SOUTH);
	}
	
	@Override
	public void actionPerformed(ActionEvent event) 
	{
		if (event.getSource()==b1) 
		{
			myCanvas.setOption(1);
		}
		else 
		if (event.getSource()==b2) 
		{
			myCanvas.setOption(2);
		}
		if (event.getSource()==text) 
		{
			String s=text.getText();
			myCanvas.setText(s);
			myCanvas.setOption(3);
		}
	}

//	Create panel
//	addButton
//	addElements	
//	North 
//	South
//	Center
}
