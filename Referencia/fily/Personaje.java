import java.awt.*;
import java.util.ArrayList; 
import java.util.Random; 
 
public abstract class Personaje implements PersonajeState
{ 
    protected int width; 
    protected int height; 
    protected int posX; 
    protected int posY; 
    protected int orientacion; 
    protected Color color; 
    protected boolean colision; 
    protected PersonajeState moviendose; 
    protected PersonajeState saltando; 
    protected PersonajeState parado; 
    protected PersonajeState currentState;  
    protected int accY, gravity;
     
    public Personaje() 
    { 
        width = 10; 
        height = 10; 
        posX = 100; 
        posY = 100; 
        /*color = new Color(200,200,200);*/ 
        color = Color.BLUE; 
        colision = true; 
        moviendose=new PSMove(this); 
        saltando=new PSJump(this); 
        parado=new PSStill(this); 
        currentState=parado; 
        accY=0;
        gravity=1;
        
    } 
     
    public Personaje(int w, int h, int x, int y, boolean col, PersonajeState m, PersonajeState s, PersonajeState p, PersonajeState c) 
    { 
        width = w; 
        height = h; 
        posX = x; 
        posY = y; 
        /*color = new Color(200,200,200);*/ 
        color = new Color(31,244,247); 
        colision = col; 
        moviendose=new PSMove(this); 
        saltando=new PSJump(this); 
        parado=new PSStill(this); 
        currentState=parado; 
        accY=0;
        gravity=1;
    } 
 
    public abstract void pintate(Graphics g); 
 
    public int getPosX() { 
        return posX; 
    } 
 
    public int getPosY() { 
        return posY; 
    } 
 
    public void setPosX(int posX) { 
        this.posX = posX; 
    } 
 
    public void setPosY(int posY) { 
        this.posY = posY; 
    } 
     
    public Color getColor(){ 
      return color; 
    } 
     
    public void setColor(Color color){ 
      this.color = color; 
    }

	public int getWidth() {
		return width;
	}

	public void setWidth(int width) {
		this.width = width;
	}

	public int getHeight() {
		return height;
	}

	public void setHeight(int height) {
		this.height = height;
	}

	public int getOrientacion() {
		return 0;
	}

	public Object getPStateStill() {
		// TODO Auto-generated method stub
		return null;
	}

	public void setState(Object pStateStill) {
		// TODO Auto-generated method stub
		
	}

	public void movex(int x) {
		// TODO Auto-generated method stub
		this.setPosX(getPosX()+x);
	} 
	
	public Rectangle getRectangle() {
		  return new Rectangle(posX,posY,width,height);
		 }
    
}
