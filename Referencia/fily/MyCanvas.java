/*
Filiberto Francisco Vázquez Rodríguez
A01206199
04/04/16	
*/

import java.awt.Canvas;
import java.awt.Color;
import java.awt.Graphics;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;

public class MyCanvas extends Canvas implements MouseListener
{
//  Implement
//	Instance variables
	private String text;
	private int option;
	
	public MyCanvas()
	{
		super();
		text = "";
		addMouseListener(this);
	}
	
	//Methods:
	public void paint(Graphics g)
	{
		if (option==1)
		{
			g.setColor(Color.BLUE);
		}
		else
		if(option==2)
		{
			g.setColor(Color.BLUE);
		}
		else
		if(option==3)
		{
			g.drawString(text,70,70);
		}	
	}
	
	//Getters:
	public String getText() {return text;}

	public int getOption() {return option;}
	
	//Setters:
	public void setText(String text) {this.text = text;}

	public void setOption(int option) 
	{this.option = option;
	repaint();}

	
	@Override
	public void mouseClicked(MouseEvent e) 
	{
		repaint();
	}
	
	public void mousePressed(MouseEvent e)
	{/* TODO Auto-generated method stub*/}

	public void mouseReleased(MouseEvent e) 
	{/* TODO Auto-generated method stub*/}

	public void mouseEntered(MouseEvent e) 
	{/* TODO Auto-generated method stub*/}

	public void mouseExited(MouseEvent e) 
	{/* TODO Auto-generated method stub*/}
	
}
