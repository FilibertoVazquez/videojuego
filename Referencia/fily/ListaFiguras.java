import java.awt.Graphics;
import java.util.LinkedList;

import java.util.ArrayList;

public class ListaFiguras {

	private ArrayList<Figura>figuras;
	
	public ListaFiguras(){
		figuras = new ArrayList<Figura>();
	}

	public ArrayList<Figura> getFiguras() {
		return figuras;
	}

	public void setFiguras(ArrayList<Figura> figuras) {
		this.figuras = figuras;
	}
	
	public void agregaFigura(Figura f){
		figuras.add(f);
	}
	
	public void paint(Graphics g){
		for(Figura f: figuras){
			f.pintate(g);
		}
	}
	
	public boolean colisiones(Personaje kate, int  caso)
	{	/*
    	for(Figura f: figuras)
    	{
			if (caso==1)//DERECHA
			{
				if(kate.getPosX()+kate.getWidth()<800){
					if(kate.getPosY()+kate.getWidth()<=f.getPosY()||kate.getPosY()>=f.getPosY()+f.getHeight()){kate.setPosX(kate.getPosX());}
					else{	
						if(kate.getPosX()+kate.getWidth()<f.getPosX()){kate.setPosX(kate.getPosX());}
						else{	
							if(kate.getPosX()+kate.getWidth()<f.getPosX()){kate.setPosX(f.getPosX()-kate.getWidth());}
							else if(kate.getPosX()>=f.getPosX()+f.getWidth()){kate.setPosX(kate.getPosX());}
						}
					}	
				}
				else{kate.setPosX(800-kate.getWidth());}
			} 
			else
			if (caso==2)//IZQUIERDA
			{
				if(kate.getPosX()>0)
				{
					if(kate.getPosY()+kate.getWidth()<=f.getPosY()||kate.getPosY()>=f.getPosY()+f.getHeight()){kate.setPosX(kate.getPosX());}
					else{	
						if(kate.getPosX()<f.getPosX()){kate.setPosX(kate.getPosX());}
						else{	
							if(kate.getPosX()>f.getPosX()+f.getWidth()){kate.setPosX(kate.getPosX());}
							else{kate.setPosX(f.getPosX()+f.getWidth());}
						}
					}	
				}
				else{kate.setPosX(0);}
			} 	
			else
			if (caso==3)//ARRIBA
			{
				if(kate.getPosY()>0)
				{
					if(kate.getPosX()+kate.getWidth()<=f.getPosX()||kate.getPosX()>=f.getPosX()+f.getWidth()){kate.setPosY(kate.getPosY());}
					else
					{	
						if(kate.getPosY()<f.getPosY()){kate.setPosY(kate.getPosY());}
						else{	
							if(kate.getPosY()>f.getPosY()+f.getHeight()){kate.setPosY(kate.getPosY());}
							else{kate.setPosY(f.getPosY()+f.getHeight());}
						}
					}	
				}
				else{kate.setPosY(0);}
			} 
			else
			if (caso==4){//ABAJO
				if(kate.getPosY()+kate.getWidth()<600){
					if(kate.getPosX()+kate.getWidth()<=f.getPosX()||kate.getPosX()>=f.getPosX()+f.getWidth())
					{kate.move();}
					else{	
						if(kate.getPosY()+kate.getWidth()<f.getPosY())
						{kate.move();}
						else{	
							if(kate.getPosY()+kate.getWidth()<f.getPosY()){kate.setPosY(f.getPosY()-kate.getWidth());}
							else if(kate.getPosY()>=f.getPosY()+f.getHeight()){kate.move();}
						}
					}	
				}
				else{kate.setPosY(600-kate.getWidth());}
			} 
	    	
    	}*/
		
		for(Figura f: figuras)
		{
			if(f.intersects(kate)==false)
			{
				return true;
			}
			else
			{
				return false;
			}
		}
		
		return false;
		
	}
	
}
