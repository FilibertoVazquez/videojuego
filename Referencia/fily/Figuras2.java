import java.awt.Color;
import java.awt.Graphics;

public class Figuras2 extends Figura{

	public Figuras2(int w, int h, int x, int y, boolean col) 
	{
		super(w, h, x, y, col);
	}
	
	public Figuras2(int w, int h, int x, int y)
	{
		super(w,h,x,y);
	}

	@Override
	public void pintate(Graphics g) 
	{
		color = new Color(145,30,248);
		g.setColor(color);
	    g.fillRect(this.getPosX(), getPosY(), this.getWidth(), this.getHeight());
	}
}
