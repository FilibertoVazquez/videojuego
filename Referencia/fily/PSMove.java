import java.awt.Graphics; 
 
public class PSMove implements PersonajeState { 
  Personaje context; 
   
  int count = 1; 
   
  public PSMove(Personaje context){ 
    this.context = context; 
  } 
   
  @Override 
  public void still() { 
    context.setState(context.getPStateStill()); 
  } 
 
  @Override 
  public void move() { 
    if (context.getPosX()>=0 && context.getPosX() <= 800) { 
      context.setPosX(context.getPosX() + context.getOrientacion()); 
    } else { 
      if (context.getPosX()<0) { 
        context.setPosX(0); 
      } else { 
        context.setPosX(800); 
      } 
    } 
 
  } 
 
  @Override 
  public void movex(int x) {  
  } 
 
  @Override 
  public void movey() { 
    // TODO Auto-generated method stub 
 
  } 
 
  @Override 
  public void jump() { 
    // TODO Auto-generated method stub 
 
  } 
 
  @Override 
  public void fall() { 
    // TODO Auto-generated method stub 
 
  } 
 
  @Override 
  public void dir() { 
    // TODO Auto-generated method stub 
 
  } 
 
  @Override 
  public void checkState() { 
    // TODO Auto-generated method stub 
  } 
 
  @Override 
  public void draw(Graphics g) { 
    // TODO Auto-generated method stub 
     
  } 
 
} 
