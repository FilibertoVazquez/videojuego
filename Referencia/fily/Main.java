import javax.swing.JFrame;

public class Main
{
	public static void main(String[] args)
	{
//      Create Window:
		MyWindow mw= new MyWindow();
//		Size:
		mw.setSize(700,700);
//		Visible:
		mw.setVisible(true);
//		Close on default:
		mw.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
	}

}
