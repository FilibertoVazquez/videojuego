import java.awt.Graphics; 
import java.util.ArrayList; 
 
public interface PersonajeState { 
   public void still(); 
   public void move(); 
   public void movex(int x); 
   public void movey(); 
   public void jump(); 
   public void fall(); 
   public void dir(); 
   public void draw(Graphics g); 
   public void checkState(); 
} 
