import java.awt.Graphics;

public class Figuras extends Figura
{
	public Figuras(int w, int h, int x, int y, boolean col) 
	{
		super(w, h, x, y, col);
	}
	
	public Figuras(int w, int h, int x, int y)
	{
		super(w,h,x,y);
	}

	@Override
	public void pintate(Graphics g) 
	{
		g.setColor(color);
	    g.fillRect(this.getPosX(), getPosY(), this.getWidth(), this.getHeight());
	}
}
