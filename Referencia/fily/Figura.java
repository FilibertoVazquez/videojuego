import java.awt.*;
import java.util.Random;

public abstract class Figura
{
    protected int width;
    protected int height;
    protected int posX;
    protected int posY;
    protected Color color;
    protected boolean colision;

    public Figura(int w, int h, int x, int y, boolean col)
    {
        width = w;
        height = h;
        posX = x;
        posY = y;
        /*color = new Color(200,200,200);*/
        color = new Color(31,244,247);
        colision = col;
    }
    
    public Figura(int w, int h, int x, int y){
    	width = w;
    	height = h;
    	posX = x;
    	posY = y;
    	color = new Color(200,200,200);
    	colision = true;
    }

    public abstract void pintate(Graphics g);


    public int getPosX() {
        return posX;
    }

    public int getPosY() {
        return posY;
    }

    public void setPosX(int posX) {
        this.posX = posX;
    }

    public void setPosY(int posY) {
        this.posY = posY;
    }
    
    public Color getColor(){
    	return color;
    }
    
    public void setColor(Color color){
    	this.color = color;
    }

	public int getWidth() {
		return width;
	}

	public void setWidth(int width) {
		this.width = width;
	}

	public int getHeight() {
		return height;
	}

	public void setHeight(int height) {
		this.height = height;
	}

	public boolean isColision() {
		return colision;
	}

	public void setColision(boolean colision) {
		this.colision = colision;
	}
	public Rectangle getRectangle() {
		  return new Rectangle(posX,posY,width,height);
		 }
	public boolean intersects(Personaje kate) {
		  Rectangle r1 = getRectangle();
		  Rectangle r2 = kate.getRectangle();
		  return r1.intersects(r2);
		 }
    
    
}
